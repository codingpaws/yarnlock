package yarnlock

import (
	"fmt"
	"io"
	"time"

	"gitlab.com/codingpaws/yarnlock/ast"
	"gitlab.com/codingpaws/yarnlock/lexer"
	"gitlab.com/codingpaws/yarnlock/parser"
)

var debug = false

func instrument[T any](name string, f func() (T, error)) (T, error) {
	start := time.Now()
	result, err := f()
	if debug {
		fmt.Printf("%s: %s\n", name, time.Since(start))
	}
	return result, err
}

type File parser.File

func Parse(lockfile io.Reader) (*File, error) {
	return instrument("yarnlock", func() (*File, error) {
		tokens, err := instrument("lexer", func() ([]lexer.Token, error) {
			return lexer.Lex(lockfile)
		})
		if err != nil {
			return nil, err
		}

		astFile, err := instrument("ast", func() (*ast.File, error) {
			return ast.Parse(tokens)
		})
		if err != nil {
			return nil, err
		}

		file, err := instrument("parser", func() (*parser.File, error) {
			return parser.Parse(astFile)
		})
		if err != nil {
			return nil, err
		}

		return (*File)(file), nil
	})
}
