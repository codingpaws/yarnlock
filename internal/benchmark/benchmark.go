package main

import (
	"bytes"
	"io"
	"log"
	"net/http"
	"os"
	"runtime/pprof"

	"gitlab.com/codingpaws/yarnlock"
)

func main() {
	out, err := os.OpenFile("cpu.pprof", os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		log.Fatalln(err)
	}
	defer out.Close()

	req, err := http.Get("https://gitlab.com/gitlab-org/gitlab/-/raw/master/yarn.lock")
	if err != nil {
		log.Fatalln(err)
	}
	if req.StatusCode != http.StatusOK {
		log.Fatalf("status code error: %d %s", req.StatusCode, req.Status)
	}

	lockfile, err := io.ReadAll(req.Body)
	if err != nil {
		log.Fatalln(err)
	}

	largeLock := []byte{}
	for i := 0; i < 200; i++ {
		largeLock = append(largeLock, lockfile...)
		largeLock = append(largeLock, []byte("\n")...)
	}

	pprof.StartCPUProfile(out)

	_, err = yarnlock.Parse(bytes.NewReader(largeLock))
	pprof.StopCPUProfile()
	if err != nil {
		log.Fatalln(err)
	}
}
