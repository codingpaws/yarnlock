package yarnlock_test

import (
	"bytes"
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/codingpaws/yarnlock"
	"gitlab.com/codingpaws/yarnlock/parser"
)

type TestDependency struct {
	Labels               []string
	Version              string
	URL                  string
	Integrity            string
	Dependencies         map[string]string
	OptionalDependencies map[string]string
}

var expectedDependencies = []TestDependency{
	{Labels: []string{"@rollup/rollup-android-arm-eabi@4.13.0", "@test/rollup-android@1.0"}, Version: "4.13.0", URL: "https://registry.yarnpkg.com/@rollup/rollup-android-arm-eabi/-/rollup-android-arm-eabi-4.13.0.tgz#b98786c1304b4ff8db3a873180b778649b5dff2b", Integrity: "sha512-5ZYPOuaAqEH/W3gYsRkxQATBW3Ii1MfaT4EQstTnLKViLi2gLSQmlmtTpGucNP3sXEpOiI5tdGhjdE111ekyEg==", Dependencies: nil},
	{Labels: []string{"@rollup/rollup-android-arm64@4.13.0"}, Version: "4.13.0", URL: "https://registry.yarnpkg.com/@rollup/rollup-android-arm64/-/rollup-android-arm64-4.13.0.tgz#8833679af11172b1bf1ab7cb3bad84df4caf0c9e", Integrity: "sha512-BSbaCmn8ZadK3UAQdlauSvtaJjhlDEjS5hEVVIN3A4bbl3X+otyf/kOJV08bYiRxfejP3DXFzO2jz3G20107+Q==", Dependencies: nil},
	{Labels: []string{"@rollup/rollup-darwin-arm64@4.13.0"}, Version: "4.13.0", URL: "https://registry.yarnpkg.com/@rollup/rollup-darwin-arm64/-/rollup-darwin-arm64-4.13.0.tgz#ef02d73e0a95d406e0eb4fd61a53d5d17775659b", Integrity: "sha512-Ovf2evVaP6sW5Ut0GHyUSOqA6tVKfrTHddtmxGQc1CTQa1Cw3/KMCDEEICZBbyppcwnhMwcDce9ZRxdWRpVd6g==", Dependencies: nil},
	{Labels: []string{"@rollup/rollup-darwin-x64@4.13.0"}, Version: "4.13.0", URL: "https://registry.yarnpkg.com/@rollup/rollup-darwin-x64/-/rollup-darwin-x64-4.13.0.tgz#3ce5b9bcf92b3341a5c1c58a3e6bcce0ea9e7455", Integrity: "sha512-U+Jcxm89UTK592vZ2J9st9ajRv/hrwHdnvyuJpa5A2ngGSVHypigidkQJP+YiGL6JODiUeMzkqQzbCG3At81Gg==", Dependencies: nil},
	{Labels: []string{"@rollup/rollup-linux-arm-gnueabihf@4.13.0"}, Version: "4.13.0", URL: "https://registry.yarnpkg.com/@rollup/rollup-linux-arm-gnueabihf/-/rollup-linux-arm-gnueabihf-4.13.0.tgz#3d3d2c018bdd8e037c6bfedd52acfff1c97e4be4", Integrity: "sha512-8wZidaUJUTIR5T4vRS22VkSMOVooG0F4N+JSwQXWSRiC6yfEsFMLTYRFHvby5mFFuExHa/yAp9juSphQQJAijQ==", Dependencies: nil},
	{Labels: []string{"@rollup/rollup-linux-arm64-gnu@4.13.0"}, Version: "4.13.0", URL: "https://registry.yarnpkg.com/@rollup/rollup-linux-arm64-gnu/-/rollup-linux-arm64-gnu-4.13.0.tgz#5fc8cc978ff396eaa136d7bfe05b5b9138064143", Integrity: "sha512-Iu0Kno1vrD7zHQDxOmvweqLkAzjxEVqNhUIXBsZ8hu8Oak7/5VTPrxOEZXYC1nmrBVJp0ZcL2E7lSuuOVaE3+w==", Dependencies: nil},
	{Labels: []string{"@rollup/rollup-linux-arm64-musl@4.13.0"}, Version: "4.13.0", URL: "https://registry.yarnpkg.com/@rollup/rollup-linux-arm64-musl/-/rollup-linux-arm64-musl-4.13.0.tgz#f2ae7d7bed416ffa26d6b948ac5772b520700eef", Integrity: "sha512-C31QrW47llgVyrRjIwiOwsHFcaIwmkKi3PCroQY5aVq4H0A5v/vVVAtFsI1nfBngtoRpeREvZOkIhmRwUKkAdw==", Dependencies: nil},
	{Labels: []string{"@rollup/rollup-linux-riscv64-gnu@4.13.0"}, Version: "4.13.0", URL: "https://registry.yarnpkg.com/@rollup/rollup-linux-riscv64-gnu/-/rollup-linux-riscv64-gnu-4.13.0.tgz#303d57a328ee9a50c85385936f31cf62306d30b6", Integrity: "sha512-Oq90dtMHvthFOPMl7pt7KmxzX7E71AfyIhh+cPhLY9oko97Zf2C9tt/XJD4RgxhaGeAraAXDtqxvKE1y/j35lA==", Dependencies: nil},
	{Labels: []string{"@rollup/rollup-linux-x64-gnu@4.13.0"}, Version: "4.13.0", URL: "https://registry.yarnpkg.com/@rollup/rollup-linux-x64-gnu/-/rollup-linux-x64-gnu-4.13.0.tgz#f672f6508f090fc73f08ba40ff76c20b57424778", Integrity: "sha512-yUD/8wMffnTKuiIsl6xU+4IA8UNhQ/f1sAnQebmE/lyQ8abjsVyDkyRkWop0kdMhKMprpNIhPmYlCxgHrPoXoA==", Dependencies: nil},
	{Labels: []string{"@rollup/rollup-linux-x64-musl@4.13.0"}, Version: "4.13.0", URL: "https://registry.yarnpkg.com/@rollup/rollup-linux-x64-musl/-/rollup-linux-x64-musl-4.13.0.tgz#d2f34b1b157f3e7f13925bca3288192a66755a89", Integrity: "sha512-9RyNqoFNdF0vu/qqX63fKotBh43fJQeYC98hCaf89DYQpv+xu0D8QFSOS0biA7cGuqJFOc1bJ+m2rhhsKcw1hw==", Dependencies: nil},
	{Labels: []string{"@rollup/rollup-win32-arm64-msvc@4.13.0"}, Version: "4.13.0", URL: "https://registry.yarnpkg.com/@rollup/rollup-win32-arm64-msvc/-/rollup-win32-arm64-msvc-4.13.0.tgz#8ffecc980ae4d9899eb2f9c4ae471a8d58d2da6b", Integrity: "sha512-46ue8ymtm/5PUU6pCvjlic0z82qWkxv54GTJZgHrQUuZnVH+tvvSP0LsozIDsCBFO4VjJ13N68wqrKSeScUKdA==", Dependencies: nil},
	{Labels: []string{"@rollup/rollup-win32-ia32-msvc@4.13.0"}, Version: "4.13.0", URL: "https://registry.yarnpkg.com/@rollup/rollup-win32-ia32-msvc/-/rollup-win32-ia32-msvc-4.13.0.tgz#a7505884f415662e088365b9218b2b03a88fc6f2", Integrity: "sha512-P5/MqLdLSlqxbeuJ3YDeX37srC8mCflSyTrUsgbU1c/U9j6l2g2GiIdYaGD9QjdMQPMSgYm7hgg0551wHyIluw==", Dependencies: nil},
	{Labels: []string{"@rollup/rollup-win32-x64-msvc@4.13.0"}, Version: "4.13.0", URL: "https://registry.yarnpkg.com/@rollup/rollup-win32-x64-msvc/-/rollup-win32-x64-msvc-4.13.0.tgz#6abd79db7ff8d01a58865ba20a63cfd23d9e2a10", Integrity: "sha512-UKXUQNbO3DOhzLRwHSpa0HnhhCgNODvfoPWv2FCXme8N/ANFfhIPMGuOT+QuKd16+B5yxZ0HdpNlqPvTMS1qfw==", Dependencies: nil},
	{Labels: []string{"@types/estree@1.0.5"}, Version: "1.0.5", URL: "https://registry.yarnpkg.com/@types/estree/-/estree-1.0.5.tgz#a6ce3e556e00fd9895dd872dd172ad0d4bd687f4", Integrity: "sha512-/kYRxGDLWzHOB7q+wtSUQlFrtcdUccpfy+X+9iMBpHK8QLLhx2wIPYuS5DYtR9Wa/YlZAbIovy7qVdB1Aq6Lyw==", Dependencies: nil},
	{Labels: []string{"fsevents@~2.3.2"}, Version: "2.3.3", URL: "https://registry.yarnpkg.com/fsevents/-/fsevents-2.3.3.tgz#cac6407785d03675a2a5e1a5305c697b347d90d6", Integrity: "sha512-5xoDfX+fL7faATnagmWPpbFtwh/R77WmMMqqHGS65C3vvB0YHrgF+B1YmZ3441tMj5n63k0212XNoJwzlhffQw==", Dependencies: nil},
	{Labels: []string{"rollup@^4.13.0"}, Version: "4.13.0", URL: "https://registry.yarnpkg.com/rollup/-/rollup-4.13.0.tgz#dd2ae144b4cdc2ea25420477f68d4937a721237a", Integrity: "sha512-3YegKemjoQnYKmsBlOHfMLVPPA5xLkQ8MHLLSw/fBrFaVkEayL51DilPpNNLq1exr98F2B1TzrV0FUlN3gWRPg==", Dependencies: map[string]string{"@types/estree": "1.0.5"}, OptionalDependencies: map[string]string{
		"@rollup/rollup-android-arm-eabi":    "4.13.0",
		"@rollup/rollup-android-arm64":       "4.13.0",
		"@rollup/rollup-darwin-arm64":        "4.13.0",
		"@rollup/rollup-darwin-x64":          "4.13.0",
		"@rollup/rollup-linux-arm-gnueabihf": "4.13.0",
		"@rollup/rollup-linux-arm64-gnu":     "4.13.0",
		"@rollup/rollup-linux-arm64-musl":    "4.13.0",
		"@rollup/rollup-linux-riscv64-gnu":   "4.13.0",
		"@rollup/rollup-linux-x64-gnu":       "4.13.0",
		"@rollup/rollup-linux-x64-musl":      "4.13.0",
		"@rollup/rollup-win32-arm64-msvc":    "4.13.0",
		"@rollup/rollup-win32-ia32-msvc":     "4.13.0",
		"@rollup/rollup-win32-x64-msvc":      "4.13.0",
		"fsevents":                           "~2.3.2",
	}},
}

func TestParse(t *testing.T) {
	require := require.New(t)
	lockfile, err := os.Open("fixtures/yarn.lock")
	defer lockfile.Close()
	require.NoError(err)

	yarnlock, err := yarnlock.Parse(lockfile)
	require.NoError(err)

	actualDependencies := []TestDependency{}
	for _, dep := range yarnlock.Dependencies {
		actualDependencies = append(actualDependencies, convertToTestDependency(dep))
	}

	require.Equal(actualDependencies, expectedDependencies)
}

func convertToTestDependency(dep parser.Dependency) TestDependency {
	return TestDependency{
		Labels:               dep.Labels,
		Version:              dep.Version,
		URL:                  dep.URL.String(),
		Integrity:            dep.Integrity,
		Dependencies:         dep.Dependencies,
		OptionalDependencies: dep.OptionalDependencies,
	}
}

func Benchmark(b *testing.B) {
	require := require.New(b)
	lockfile, err := os.Open("fixtures/yarn.lock")
	require.NoError(err)
	defer lockfile.Close()
	contents, err := io.ReadAll(lockfile)
	require.NoError(err)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := yarnlock.Parse(bytes.NewReader(contents))
		require.NoError(err)
	}
}
