package ast

import (
	"fmt"
	"strings"

	"gitlab.com/codingpaws/yarnlock/lexemes"
	"gitlab.com/codingpaws/yarnlock/lexer"
)

type File struct {
	Blocks []Block
}

type Block struct {
	Line   int
	Labels []string
	Keys   map[string]string
	Blocks map[string]Block
}

type parser struct {
	tokens        []lexer.Token
	index         int
	currentIndent int
}

func (self *parser) Peek() lexer.Token {
	if self.index >= len(self.tokens) {
		return lexer.Token{Type: lexemes.EOF}
	}

	return self.tokens[self.index]
}

func (self *parser) PeekIs(typ lexemes.Lexeme) bool {
	token := self.Peek()
	return token.Type == typ
}

func (self *parser) Consume() lexer.Token {
	if self.index >= len(self.tokens) {
		return lexer.Token{Type: lexemes.EOF}
	}
	token := self.tokens[self.index]
	self.index++
	return token
}

func (self *parser) Lex() (*File, error) {
	var file File
	for {
		if self.PeekIs(lexemes.EOF) {
			return &file, nil
		}
		block, err := self.parseBlock()
		if err != nil {
			return nil, err
		}

		file.Blocks = append(file.Blocks, *block)
	}
}
func (self *parser) parseBlock() (*Block, error) {
	for self.PeekIs(lexemes.Comment) || self.PeekIs(lexemes.Empty) {
		self.index++
	}

	if !self.PeekIs(lexemes.Block) {
		return nil, fmt.Errorf("expected block")
	}

	blockToken := self.Consume()

	if blockToken.Indent != self.currentIndent {
		return nil, self.NewError("expected indent %d but got %d", self.currentIndent, blockToken.Indent)
	}

	var block = Block{
		Line:   self.index,
		Keys:   make(map[string]string),
		Blocks: make(map[string]Block),
	}
	if blockToken.Indent == 0 {
		var err error
		block.Labels, err = parseTags(blockToken.Value)
		if err != nil {
			return nil, self.NewError("parsing tags: %s", err)
		}
	}

	for {
		if self.PeekIs(lexemes.Comment) {
			self.Consume()
			continue
		}

		if self.PeekIs(lexemes.Line) {
			line := self.Consume()
			if line.Indent != self.currentIndent+1 {
				return nil, self.NewError("expected indent %d but got %d", self.currentIndent, line.Indent)
			}
			idx := strings.Index(line.Value, " ")
			if idx == -1 {
				return nil, self.NewError("expected key and value but got %s", line.Value)
			}

			key, err := parseKeywordOrStringLiteral(line.Value[:idx])
			if err != nil {
				return nil, self.NewError("parsing key: %s", err)
			}

			value, err := parseKeywordOrStringLiteral(line.Value[idx+1:])
			if err != nil {
				return nil, self.NewError("parsing value: %s", err)
			}

			block.Keys[key] = value
		} else if self.PeekIs(lexemes.Block) {
			self.currentIndent++
			subBlockToken := self.Peek()
			if subBlockToken.Indent < self.currentIndent {
				self.currentIndent--
				return &block, nil
			}
			subBlock, err := self.parseBlock()
			if err != nil {
				return nil, err
			}
			block.Blocks[subBlockToken.Value] = *subBlock
			self.currentIndent--
		} else if self.PeekIs(lexemes.EOF) {
			return &block, nil
		} else if self.PeekIs(lexemes.Empty) {
			// TODO more than one empty means the block is over!
			self.index++
		} else {
			token := self.Consume()
			return nil, self.NewError("expected line or sub-block within block but got %s", token.Type)
		}
	}
}

func parseKeywordOrStringLiteral(part string) (string, error) {
	if len(part) == 0 {
		return "", fmt.Errorf("parsing string literal or keyword: empty")
	}

	if part[0] != '"' && part[0] != '\'' {
		return part, nil
	}

	return parseStringLiteral(part)
}

func parseStringLiteral(part string) (string, error) {
	if len(part) == 0 {
		return "", fmt.Errorf("parsing string literal: empty")
	}

	if part[0] != '"' {
		return "", fmt.Errorf("parsing string literal: missing opening quote")
	}

	if part[len(part)-1] != '"' {
		return "", fmt.Errorf("parsing string literal: missing closing quote")
	}
	literalContent := part[1 : len(part)-1]

	isEscape := false
	for i := 0; i < len(literalContent); i++ {
		if literalContent[i] == '\\' && !isEscape {
			isEscape = true
			continue
		}

		if literalContent[i] == '"' && !isEscape {
			return "", fmt.Errorf("parsing string literal: unterminated quote")
		}
		isEscape = false
	}

	return literalContent, nil
}

func parseTags(line string) ([]string, error) {
	tags := []string{}

	isString := false
	builder := new(strings.Builder)
	for i := 0; i < len(line); i++ {
		c := line[i]
		if !isString && builder.Len() == 0 && c == ' ' {
			continue
		}
		if c == '"' {
			if builder.Len() > 0 && !isString {
				return nil, fmt.Errorf("parsing tags: quotes outside of string literal")
			}
			if builder.Len() == 0 {
				isString = true
			} else {
				isString = false
				tags = append(tags, builder.String())
				builder.Reset()
				if i+1 >= len(line) {
					continue
				}
				if line[i+1] != ',' {
					return nil, fmt.Errorf("parsing tags: expected comma after string literal")
				}
				i++
			}
			continue
		}

		if c == ',' && !isString {
			tags = append(tags, builder.String())
			builder.Reset()
			continue
		}

		builder.WriteByte(c)
	}

	if builder.Len() > 0 {
		tags = append(tags, builder.String())
	}

	if isString {
		return nil, fmt.Errorf("parsing tags: unterminated string")
	}

	return tags, nil
}

func (self *parser) NewError(format string, data ...any) error {
	message := fmt.Sprintf(format, data...)
	return fmt.Errorf("line %d: %s", self.index, message)
}

func Parse(tokens []lexer.Token) (*File, error) {
	parser := parser{tokens: tokens}
	return parser.Lex()
}
