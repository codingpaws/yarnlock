package lexemes

type Lexeme int

const (
	Empty Lexeme = iota
	Comment
	Line
	Block
	EOF
)

func (t Lexeme) String() string {
	switch t {
	case Empty:
		return "EMPTY"
	case Line:
		return "LINE"
	case Comment:
		return "COMMENT"
	case Block:
		return "BLOCK"
	case EOF:
		return "EOF"
	}
	return "INVALID"
}
