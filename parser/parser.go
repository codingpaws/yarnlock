package parser

import (
	"fmt"
	"net/url"
	"strings"

	"gitlab.com/codingpaws/yarnlock/ast"
)

type File struct {
	Dependencies []Dependency
}

type Dependency struct {
	Labels               []string
	Version              string
	URL                  *url.URL
	Integrity            string
	Dependencies         map[string]string
	OptionalDependencies map[string]string
}

func (self *Dependency) CacheKey() string {
	label := self.Labels[0]
	idx := strings.LastIndex(label, "@")
	label = strings.ReplaceAll(label[:idx], "/", "-")
	return fmt.Sprintf("%s-%s", label, self.URL.Fragment)
}

func Parse(astFile *ast.File) (file *File, err error) {
	file = &File{}
	for _, block := range astFile.Blocks {
		dependency := Dependency{
			Labels: block.Labels,
		}

		var ok bool
		dependency.Integrity, ok = block.Keys["integrity"]
		if !ok {
			return nil, newError(block, "missing integrity")
		}
		resolved, ok := block.Keys["resolved"]
		if !ok {
			return nil, newError(block, "missing resolved")
		}
		dependency.URL, err = url.Parse(resolved)
		if err != nil {
			return nil, newError(block, "parsing resolved: %w", err)
		}
		dependency.Version, ok = block.Keys["version"]
		if !ok {
			return nil, newError(block, "missing version")
		}

		if block.Blocks != nil {
			dependencies, ok := block.Blocks["dependencies"]
			if ok {
				dependency.Dependencies = dependencies.Keys
			}
			optionalDependencies, ok := block.Blocks["optionalDependencies"]
			if ok {
				dependency.OptionalDependencies = optionalDependencies.Keys
			}
		}
		file.Dependencies = append(file.Dependencies, dependency)
	}
	return file, nil
}

func newError(block ast.Block, format string, data ...any) error {
	return fmt.Errorf("line %d: %s", block.Line, fmt.Errorf(format, data...))
}
