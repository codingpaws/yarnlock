package lexer

import (
	"bufio"
	"io"
	"strings"

	"gitlab.com/codingpaws/yarnlock/lexemes"
)

type Token struct {
	Type   lexemes.Lexeme
	Indent int
	Value  string
}

func Lex(r io.Reader) (tokens []Token, err error) {
	reader := bufio.NewReader(r)
	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				return tokens, nil
			}
			return nil, err
		}
		line = strings.TrimRight(line, "\n\r\t ")

		var typ lexemes.Lexeme
		var indent int

		for strings.HasPrefix(line, "  ") {
			line = line[2:]
			indent++
		}

		if len(line) == 0 {
			typ = lexemes.Empty
		} else if strings.HasPrefix(line, "#") {
			typ = lexemes.Comment
		} else if strings.HasSuffix(line, ":") {
			typ = lexemes.Block
			line = line[:len(line)-1]
		} else if len(strings.TrimSpace(line)) == 0 {
			typ = lexemes.Empty
		} else {
			typ = lexemes.Line
		}

		tokens = append(tokens, Token{
			Type:   typ,
			Indent: indent,
			Value:  line,
		})
	}
}
